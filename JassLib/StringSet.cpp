#include "StringSet.hpp"

StringSet::StringSet(std::initializer_list<FancyString> strings) :
    strings_(std::move(strings))
{
}

StringSet::StringSet(std::unordered_set<FancyString> strings) :
    strings_(std::move(strings))
{
}

void StringSet::add(FancyString const& str)
{
    strings_.insert(str);
}

int StringSet::size() const
{
    return (int)strings_.size();
}

bool StringSet::empty() const
{
    return size() == 0;
}

bool StringSet::contains(FancyString const& str) const
{
    return strings_.count(str) >= 1;
}
