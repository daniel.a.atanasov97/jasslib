#include "Parser.hpp"

namespace Jass
{
	void TokenList::tokenize(FancyString script)
	{
		Tokenizer tokenizer(script);
		Token token = tokenizer.next();
		while (token.type() != TOKEN_EOF)
		{
			tokens_.push_back(token);
			token = tokenizer.next();
		}

		eof_ = Token("", text_size(), text_size(), TOKEN_EOF);

		// TODO@Daniel:
		// A fake newline should probably be added here
	}

	Token const& TokenList::at(int idx) const
	{
		if (idx < 0 || idx >= tokens_.size())
		{
			return eof();
		}
		return tokens_[idx];
	}

	Token const& TokenList::operator[](int idx) const
	{
		return at(idx);
	}

	Token const& TokenList::eof() const
	{
		return eof_;
	}

	int TokenList::text_size() const
	{
		return text_size_;
	}

	int TokenList::next_not_of(TokenType token_type, int idx) const
	{
		Token token = at(idx);
		while (token.type() == token_type)
		{
			idx++;
			token = at(idx);
		}
		return idx;
	}

	int TokenList::next_of(TokenType token_type, int idx) const
	{
		Token token = at(idx);
		while (token.type() != token_type && token.type() != TOKEN_EOF)
		{
			idx++;
			token = at(idx);
		}
		return idx;
	}

	int TokenList::next_non_comment(int idx) const
	{
		Token token = at(idx);
		while (token.is_comment())
		{
			idx++;
			token = at(idx);
		}
		return idx;
	}

	int TokenList::next_non_comment_or_whitespace(int idx) const
	{
		Token token = at(idx);
		while (token.is_comment() || token.is_whitespace())
		{
			idx++;
			token = at(idx);
		}
		return idx;
	}

	template<>
	Modifier::Pointer Parser::parse<Modifier>(TokenList const& tokens, int& idx)
	{
		ModifierType type;

		int modifier_idx = tokens.next_non_comment_or_whitespace(idx);
		Token modifier_token = tokens[modifier_idx];
		if (modifier_token.is_keyword("local"))
		{
			type = MOD_LOCAL;
		}
		else if (modifier_token.is_keyword("private"))
		{
			type = MOD_PRIVATE;
		}
		else if (modifier_token.is_keyword("public"))
		{
			type = MOD_PUBLIC;
		}
		else if (modifier_token.is_keyword("static"))
		{
			type = MOD_STATIC;
		}
		else if (modifier_token.is_keyword("constant"))
		{
			type = MOD_CONSTANT;
		}
		else
		{
			return nullptr;
		}

		idx = modifier_idx + 1;
		return Modifier::make(
			modifier_token.start(),
			modifier_token.stop(),
			type
		);
	}

	template<>
	ModifierList::Pointer Parser::parse<ModifierList>(TokenList const& tokens, int& idx)
	{
		return parse_list<ModifierList>(tokens, idx);
	}

	template<>
	VariableList::Pointer Parser::parse<VariableList>(TokenList const& tokens, int& idx)
	{
		int modifier_idx = tokens.next_non_comment_or_whitespace(idx);
		// Empty list is allowed so this can't fail
		ModifierList::Pointer modifiers_ptr = parse<ModifierList>(tokens, modifier_idx);

		int type_idx = tokens.next_non_comment_or_whitespace(modifier_idx);
		Token type_token = tokens[type_idx];
		if (!type_token.is_identifier())
		{
			return nullptr;
		}

		int name_idx = tokens.next_non_comment_or_whitespace(type_idx + 1);
		Token name_token = tokens[name_idx];
		if (!name_token.is_identifier())
		{
			return nullptr;
		}

		idx = name_idx + 1;
		return VariableList::make(
			modifiers_ptr->start(),
			name_token.stop(),
			std::move(modifiers_ptr),
			type_token.value(),
			name_token.value()
		);
	}

	template<>
	VariableDeclarationList::Pointer Parser::parse<VariableDeclarationList>(TokenList const& tokens, int& idx)
	{
		return parse_list<VariableDeclarationList>(tokens, idx);
	}

	template<>
	GlobalsDeclaration::Pointer Parser::parse<GlobalsDeclaration>(TokenList const& tokens, int& idx)
	{
		int start_idx = tokens.next_non_comment_or_whitespace(idx);
		Token start_token = tokens[start_idx];
		if (!start_token.is_keyword("globals"))
		{
			return nullptr;
		}

		int list_idx = start_idx + 1;
		VariableDeclarationList::Pointer list_ptr = parse<VariableDeclarationList>(tokens, list_idx);
		if (!list_ptr)
		{
			return nullptr;
		}

		int stop_idx = tokens.next_non_comment_or_whitespace(list_idx);
		Token stop_token = tokens[stop_idx];
		if (!stop_token.is_keyword("endglobals"))
		{
			return nullptr;
		}

		idx = stop_idx + 1;
		return GlobalsDeclaration::make(
			start_token.start(),
			stop_token.stop(),
			std::move(list_ptr)
		);
	}

	template<>
	TypeDeclaration::Pointer Parser::parse<TypeDeclaration>(TokenList const& tokens, int& idx)
	{
		int type_idx = tokens.next_non_comment_or_whitespace(idx);
		Token type_token = tokens[type_idx];
		if (!type_token.is_keyword("type"))
		{
			return nullptr;
		}

		int name_idx = tokens.next_non_comment_or_whitespace(type_idx + 1);
		Token name_token = tokens[name_idx];
		if (!name_token.is_identifier())
		{
			return nullptr;
		}

		int extends_idx = tokens.next_non_comment_or_whitespace(name_idx + 1);
		Token extends_token = tokens[extends_idx];
		if (!extends_token.is_keyword("extends"))
		{
			return nullptr;
		}

		int base_idx = tokens.next_non_comment_or_whitespace(extends_idx + 1);
		Token base_token = tokens[base_idx];
		if (!base_token.is_identifier())
		{
			return nullptr;
		}

		idx = base_idx + 1;
		return TypeDeclaration::make(
			type_token.start(),
			base_token.stop(),
			name_token.value(),
			base_token.value()
		);
	}

	template<>
	Scope::Pointer Parser::parse<Scope>(TokenList const& tokens, int& idx)
	{
		int start_idx = tokens.next_non_comment_or_whitespace(idx);
		Token start_token = tokens[start_idx];
		if (!start_token.is_keyword("scope") && !start_token.is_keyword("library"))
		{
			return nullptr;
		}

		int name_idx = tokens.next_non_comment_or_whitespace(start_idx + 1);
		Token name_token = tokens[name_idx];
		if (!name_token.is_identifier())
		{
			return nullptr;
		}

		int list_idx = name_idx + 1;
		DeclarationList::Pointer list_ptr = parse<DeclarationList>(tokens, list_idx);
		if (!list_ptr)
		{
			return nullptr;
		}

		int stop_idx = tokens.next_non_comment_or_whitespace(list_idx);
		Token stop_token = tokens[stop_idx];
		if (!stop_token.is_keyword("endscope") && !stop_token.is_keyword("endlibrary"))
		{
			return nullptr;
		}

		idx = stop_idx + 1;
		return Scope::make(
			start_token.start(),
			stop_token.stop(),
			name_token.value(),
			std::move(list_ptr)
		);
	}

	template<>
	Declaration::Pointer Parser::parse<Declaration>(TokenList const& tokens, int& idx)
	{
		int typedef_idx = idx;
		TypeDeclaration::Pointer typedef_ptr = parse<TypeDeclaration>(tokens, typedef_idx);
		if (typedef_ptr)
		{
			idx = typedef_idx;
			return std::move(typedef_ptr);
		}

		int scope_idx = idx;
		Scope::Pointer scope_ptr = parse<Scope>(tokens, scope_idx);
		if (scope_ptr)
		{
			idx = scope_idx;
			return std::move(scope_ptr);
		}

		int globals_idx = idx;
		GlobalsDeclaration::Pointer globals_ptr = parse<GlobalsDeclaration>(tokens, globals_idx);
		if (globals_ptr)
		{
			idx = globals_idx;
			return std::move(globals_ptr);
		}

		return nullptr;
	}

	template<>
	DeclarationList::Pointer Parser::parse<DeclarationList>(TokenList const& tokens, int& idx)
	{
		return parse_list<DeclarationList>(tokens, idx);
	}

	DeclarationList::Pointer Parser::parse(FancyString script)
	{
		TokenList tokens;
		tokens.tokenize(script);

		int idx = 0;
		return parse<DeclarationList>(tokens, idx);
	}
}