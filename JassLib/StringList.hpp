#pragma once

#include <string>
#include <vector>

#include "FancyString.hpp"

struct StringList
{
private:
    std::vector<FancyString> strings_;

public:
    StringList() = default;
    StringList(std::initializer_list<FancyString> strings);
    StringList(std::vector<FancyString> strings);

    void append(FancyString str);

    int size() const;
    bool empty() const;
    bool contains(FancyString const& str) const;

    FancyString join(FancyString const& sep) const;
    FancyString join(FancyChar sep) const;
};