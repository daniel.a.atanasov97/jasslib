#pragma once

#include <vector>
#include <string>
#include <memory>

#include "FancyChar.hpp"
#include "FancyString.hpp"
#include "StringList.hpp"

#define POINTER_MEMBERS(ValueType)			\
using Pointer = std::unique_ptr<ValueType>;	\
template <typename ...Types>				\
static Pointer make(Types&& ...args)		\
{											\
	return std::make_unique<ValueType>(		\
		std::forward<Types>(args)...		\
	);										\
}

namespace Jass
{
    // NOTE@Daniel:
    // Groundwork for the AST of the language

    // TODO@Daniel:
    // Add the rest of the language

    // TODO@Daniel:
    // Personally, I dislike the idea of having unique pointers everywhere
    // If it can be changed without hurting readability, then it probably should

    // TODO@Daniel:
    // Node structure may be incosistent and should be fixed eventually

    enum ModifierType
    {
        MOD_PRIVATE,
        MOD_PUBLIC,
        MOD_STATIC,
        MOD_CONSTANT,
        MOD_LOCAL
    };

    struct Construct
    {
    public:
        POINTER_MEMBERS(Construct);

    private:
        int start_;
        int stop_;

        Construct* parent_;

        void set_parent(Construct* parent = nullptr)
        {
            parent_ = parent;
        }

    public:
        Construct(Construct const&) = delete;
        Construct(
            int start,
            int stop,
            Construct* parent = nullptr
        ) :
            start_(start),
            stop_(stop)
        {
            set_parent(parent);
        }

        Construct& operator=(Construct const&) = delete;

        int start() const
        {
            return start_;
        }
        int stop() const
        {
            return stop_;
        }

        bool has_parent() const
        {
            return parent_ != nullptr;
        }

        Construct const* parent() const
        {
            return parent_;
        }

        virtual FancyString to_string() const = 0;

        virtual ~Construct() = default;
    };

    template <typename ConstructType>
    struct ConstructList : Construct
    {
    public:
        POINTER_MEMBERS(ConstructList<ConstructType>);

        using Child = ConstructType;
        using ChildPointer = typename ConstructType::Pointer;
        using Container = std::vector<ChildPointer>;

    protected:
        Container children_;

    public:
        ConstructList(
            int start,
            int stop,
            Container children,
            Construct* parent = nullptr
        ) :
            Construct(start, stop, parent),
            children_(std::move(children))
        {

        }

        virtual FancyString to_string() const
        {
            StringList strings;
            for (ChildPointer const& child : children_)
            {
                strings.append(child->to_string());
            }
            return strings.join('\n');
        }

        virtual ~ConstructList() = default;
    };

    struct Declaration : Construct
    {
    public:
        POINTER_MEMBERS(Declaration);

    public:
        Declaration(
            int start,
            int stop,
            Construct* parent
        ) :
            Construct(start, stop, parent)
        {
        }

        virtual FancyString to_string() const = 0;

        virtual ~Declaration() = default;
    };

    struct DeclarationList : ConstructList<Declaration>
    {
    public:
        POINTER_MEMBERS(DeclarationList);

    public:
        using ConstructList<Declaration>::ConstructList;

        virtual FancyString to_string() const
        {
            StringList strings;

            for (Declaration::Pointer const& decl : children_)
            {
                strings.append(decl->to_string());
            }

            return strings.join('\n');
        }

        virtual ~DeclarationList() = default;
    };

    struct Scope : Declaration
    {
    public:
        POINTER_MEMBERS(Scope);

    private:
        FancyString name_;

        DeclarationList::Pointer declarations_;

    public:
        Scope(
            int start,
            int stop,
            FancyString name,
            DeclarationList::Pointer declarations,
            Construct* parent = nullptr
        ) :
            Declaration(start, stop, parent),
            name_(std::move(name)),
            declarations_(std::move(declarations))
        {
        }

        virtual FancyString to_string() const
        {
            StringList first_line{
                "scope", name_
            };

            StringList strings{
                first_line.join(' '),
                declarations_->to_string(),
                "endscope"
            };
            return strings.join('\n');
        }

        virtual ~Scope() = default;
    };

    struct Modifier : Construct
    {
    public:
        POINTER_MEMBERS(Modifier);

    private:
        ModifierType type_;

    public:
        Modifier(
            int start,
            int stop,
            ModifierType type,
            Construct* parent = nullptr
        ) :
            Construct(start, stop, parent),
            type_(type)
        {
        }

        virtual FancyString to_string() const
        {
            switch (type_)
            {
            case Jass::MOD_PRIVATE:
                return "private";
            case Jass::MOD_PUBLIC:
                return "public";
            case Jass::MOD_STATIC:
                return "static";
            case Jass::MOD_CONSTANT:
                return "constant";
            case Jass::MOD_LOCAL:
                return "local";
            }
            return "";
        }

        virtual ~Modifier() = default;
    };

    struct ModifierList : ConstructList<Modifier>
    {
    public:
        POINTER_MEMBERS(ModifierList);

    public:
        using ConstructList<Modifier>::ConstructList;

        virtual FancyString to_string() const
        {
            StringList strings;
            for (ChildPointer const& child : children_)
            {
                strings.append(child->to_string());
            }
            return strings.join(' ');
        }

        virtual ~ModifierList() = default;
    };

    // TODO@Daniel:
    // Make this a statement when I implement them
    struct VariableList : Construct
    {
    public:
        POINTER_MEMBERS(VariableList);

    private:
        ModifierList::Pointer modifiers_;
        FancyString type_;
        FancyString name_;

    public:
        VariableList(
            int start,
            int stop,
            ModifierList::Pointer modifiers,
            FancyString type,
            FancyString name,
            Construct* parent = nullptr
        ) :
            Construct(start, stop, parent),
            modifiers_(std::move(modifiers)),
            type_(std::move(type)),
            name_(std::move(name))
        {
        }

        virtual FancyString to_string() const
        {
            StringList strings
            {
                modifiers_->to_string(),
                type_,
                name_,
            };

            return strings.join(' ');
        }

        virtual ~VariableList() = default;
    };

    struct VariableDeclarationList : ConstructList<VariableList>
    {
    public:
        POINTER_MEMBERS(VariableDeclarationList);

    public:
        using ConstructList<VariableList>::ConstructList;

        virtual ~VariableDeclarationList() = default;
    };

    struct GlobalsDeclaration : Declaration
    {
    public:
        POINTER_MEMBERS(GlobalsDeclaration);

    private:
        VariableDeclarationList::Pointer variables_;

    public:
        GlobalsDeclaration(
            int start,
            int stop,
            VariableDeclarationList::Pointer variables,
            Construct* parent = nullptr
        ) :
            Declaration(start, stop, parent),
            variables_(std::move(variables))
        {
        }

        virtual FancyString to_string() const
        {
            StringList strings
            {
                "globals",
                variables_->to_string(),
                "endglobals"
            };

            return strings.join('\n');
        }

        virtual ~GlobalsDeclaration() = default;
    };

    struct TypeDeclaration : Declaration
    {
    public:
        POINTER_MEMBERS(TypeDeclaration);

    private:
        FancyString name_;
        FancyString base_;

    public:
        TypeDeclaration(
            int start,
            int stop,
            FancyString name,
            FancyString base,
            Construct* parent = nullptr
        ) :
            Declaration(start, stop, parent),
            name_(std::move(name)),
            base_(std::move(base))
        {
        }

        virtual FancyString to_string() const
        {
            StringList strings{
                "type",
                name_,
                "extends",
                base_
            };

            return strings.join(' ');
        }

        virtual ~TypeDeclaration() = default;
    };
}