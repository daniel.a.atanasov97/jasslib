#include "StringList.hpp"

StringList::StringList(std::initializer_list<FancyString> strings) :
    strings_(std::move(strings))
{
}

StringList::StringList(std::vector<FancyString> strings) :
    strings_(std::move(strings))
{
}

void StringList::append(FancyString str)
{
    strings_.push_back(std::move(str));
}

int StringList::size() const
{
    return (int)strings_.size();
}

bool StringList::empty() const
{
    return size() == 0;
}

bool StringList::contains(FancyString const& str) const
{
    for (FancyString const& item : strings_)
    {
        if (item == str)
        {
            return true;
        }
    }
    return false;
}

FancyString StringList::join(FancyString const& sep) const
{
    FancyString result;

    if (!empty())
    {
        result = strings_[0];

        for (int idx = 1; idx < size(); idx++)
        {
            result += sep + strings_[idx];
        }
    }

    return result;
}

FancyString StringList::join(FancyChar sep) const
{
    return join(FancyString(1, sep));
}
