#pragma once

#include "FancyString.hpp"
#include "Tokenizer.hpp"
#include "Ast.hpp"

namespace Jass
{
	// TODO@Daniel:
	// Probably better to move it to it's own header (or maybe in JassTokenizer.h?)
	class TokenList
	{
	private:
		std::vector<Token> tokens_;
		Token eof_;

		int text_size_;

	public:
		TokenList() = default;

		Token const& at(int idx) const;
		Token const& operator[](int idx) const;
		Token const& eof() const;

		int text_size() const;

		// NOTE@Daniel:
		// next_*() functions accept the index of the first possible token
		// They return the index of the first found token

		// TODO@Daniel:
		// It may be best if they followed a similar pattern as parse_*() functions in Parser

		int next_not_of(TokenType token_type, int idx = 0) const;
		int next_of(TokenType token_type, int idx = 0) const;
		int next_non_comment(int idx = 0) const;
		int next_non_comment_or_whitespace(int idx = 0) const;

		void tokenize(FancyString script);
	};

	// NOTE@Daniel:
	// Adding simple error detection and recovery should be fairly easy
	// Some syntactic errors will be a pain, though
	class Parser {
	private:
		// NOTE@Daniel:
		// parse_*() functions take the list of tokens and the index of the first unused token
		// They return the pointer of the construct as a return type 
		// and the index of the first unused token on success
		// Otherwise, they return a null pointer and idx remains unchanged

		// TODO@Daniel:
		// Add parsing for regular JASS2
		// I'll worry about vJASS when when I get to it

		// NOTE@Daniel:
		// These only make sense being used while parsing, so I don't see a reason to define them here
		// The templates are just there to allow as much reuse as possible
		// Also, specializations are ugly

		// Most recursive non-terminals will probably be defined this way
		template <typename ListType>
		typename ListType::Pointer parse_list(TokenList const& tokens, int& idx)
		{
			typename ListType::Container children;

			int last_idx = idx;
			while (true)
			{
				typename ListType::ChildPointer decl = parse<ListType::Child>(tokens, last_idx);
				if (!decl)
				{
					break;
				}
				children.push_back(
					std::move(decl)
				);
			}

			int start;
			int stop;
			if (children.empty())
			{
				start = tokens[idx].start();
				stop = tokens[idx].start();
			}
			else
			{
				start = children.front()->start();
				stop = children.back()->stop();
			}

			idx = last_idx;
			return ListType::make(
				start,
				stop,
				std::move(children)
			);
		}

		// TODO@Daniel:
		// Have this deleted or put a static_assert() inside
		// Iirc, deleting this doesn't work correctly due to some obscure compiler bug
		// Currently, this should generate a linker error rathern than a compiler error
		template <typename NodeType>
		typename NodeType::Pointer parse(TokenList const& tokens, int& idx);

		template<>
		Modifier::Pointer parse<Modifier>(TokenList const& tokens, int& idx);
		template<>
		ModifierList::Pointer parse<ModifierList>(TokenList const& tokens, int& idx);
		template<>
		VariableList::Pointer parse<VariableList>(TokenList const& tokens, int& idx);
		template<>
		VariableDeclarationList::Pointer parse<VariableDeclarationList>(TokenList const& tokens, int& idx);
		template<>
		GlobalsDeclaration::Pointer parse<GlobalsDeclaration>(TokenList const& tokens, int& idx);
		template<>
		TypeDeclaration::Pointer parse<TypeDeclaration>(TokenList const& tokens, int& idx);
		template<>
		Scope::Pointer parse<Scope>(TokenList const& tokens, int& idx);
		template<>
		Declaration::Pointer parse<Declaration>(TokenList const& tokens, int& idx);
		template<>
		DeclarationList::Pointer parse<DeclarationList>(TokenList const& tokens, int& idx);

	public:
		Parser() = default;

		// TODO@Daniel:
		// The return type is temporary and used for testing
		DeclarationList::Pointer parse(FancyString script);
	};
}