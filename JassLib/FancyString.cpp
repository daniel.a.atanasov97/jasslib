#include "FancyString.hpp"

FancyString::FancyString(int size, FancyChar filler) :
    str_(size, filler)
{
}

FancyString::FancyString(char const* str) :
    str_(str)
{
}

FancyString::FancyString(std::string str) :
    str_(std::move(str))
{
}

int FancyString::size() const
{
    return (int)str_.size();
}

bool FancyString::empty() const
{
    return size() == 0;
}

bool FancyString::contains(FancyChar ch) const
{
    return std::find(
        std::begin(str_),
        std::end(str_),
        ch
    ) == std::end(str_);
}

FancyChar FancyString::at(int idx) const
{
    if (idx < 0 || idx >= size())
    {
        return '\0';
    }
    return str_[idx];
}

FancyChar FancyString::operator[](int idx) const
{
    if (idx < 0 || idx >= size())
    {
        return '\0';
    }
    return str_[idx];
}

FancyString FancyString::left(int stop) const
{
    return str_.substr(0, stop);
}

FancyString FancyString::middle(int start, int stop) const
{
    return str_.substr(start, stop - start);
}

FancyString FancyString::right(int start) const
{
    return str_.substr(start);
}

bool FancyString::operator<(FancyString const& other) const
{
    return str_ < other.str_;
}

bool FancyString::operator<=(FancyString const& other) const
{
    return str_ <= other.str_;
}

bool FancyString::operator>(FancyString const& other) const
{
    return str_ > other.str_;
}

bool FancyString::operator>=(FancyString const& other) const
{
    return str_ >= other.str_;
}

bool FancyString::operator==(FancyString const& other) const
{
    return str_ == other.str_;
}

FancyString FancyString::operator+(FancyString const& other) const
{
    return str_ + other.str_;
}

FancyString& FancyString::operator=(FancyString other)
{
    str_ = std::move(other.str_);
    return *this;
}

FancyString& FancyString::operator+=(FancyString const& other)
{
    str_ += other.str_;
    return *this;
}

FancyString::operator std::string() const
{
    return str_;
}

std::size_t std::hash<FancyString>::operator()(FancyString const& str) const
{
    return std::hash<std::string>()(str);
}