#include "FancyChar.hpp"

FancyChar::FancyChar(char ch) :
    ch_(ch)
{
}

bool FancyChar::is_lowercase() const
{
    return ch_ >= 'a' && ch_ <= 'z';
}

bool FancyChar::is_uppercase() const
{
    return ch_ >= 'A' && ch_ <= 'Z';
}

bool FancyChar::is_letter() const
{
    return is_lowercase() || is_uppercase();
}

bool FancyChar::is_digit() const
{
    return ch_ >= '0' && ch_ <= '9';
}

bool FancyChar::is_underscore() const
{
    return ch_ == '_';
}

bool FancyChar::is_single_quote() const
{
    return ch_ == '\'';
}

bool FancyChar::is_double_quote() const
{
    return ch_ == '"';
}

bool FancyChar::is_open_parenthesis() const
{
    return ch_ == '(';
}

bool FancyChar::is_close_parenthesis() const
{
    return ch_ == ')';
}

bool FancyChar::is_open_bracket() const
{
    return ch_ == '[';
}

bool FancyChar::is_close_bracket() const
{
    return ch_ == ']';
}

bool FancyChar::is_dot() const
{
    return ch_ == '.';
}

bool FancyChar::is_comma() const
{
    return ch_ == ',';
}

bool FancyChar::is_front_slash() const
{
    return ch_ == '/';
}

bool FancyChar::is_back_slash() const
{
    return ch_ == '\\';
}

bool FancyChar::is_plus() const
{
    return ch_ == '+';
}

bool FancyChar::is_minus() const
{
    return ch_ == '-';
}

bool FancyChar::is_star() const
{
    return ch_ == '*';
}

bool FancyChar::is_equals() const
{
    return ch_ == '=';
}

bool FancyChar::is_more() const
{
    return ch_ == '>';
}

bool FancyChar::is_less() const
{
    return ch_ == '<';
}

bool FancyChar::is_exclamation_mark() const
{
    return ch_ == '!';
}

bool FancyChar::is_pipe() const
{
    return ch_ == '|';
}

bool FancyChar::is_whitespace() const
{
    switch (ch_)
    {
    case ' ':
    case '\t':
    case '\n':
    case '\r':
        return true;
    }
    return false;
}

bool FancyChar::is_line_feed() const
{
    return ch_ == '\n';
}

bool FancyChar::is_carriage_return() const
{
    return ch_ == '\r';
}

bool FancyChar::is_newline_character() const
{
    return is_line_feed() || is_carriage_return();
}

FancyChar::operator char() const
{
    return ch_;
}

std::size_t std::hash<FancyChar>::operator()(FancyChar ch)
{
    return std::hash<char>()(ch);
}