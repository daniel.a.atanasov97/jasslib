#pragma once

#include <functional>

struct FancyChar
{
private:
    char ch_;

public:
    FancyChar() = default;
    FancyChar(char ch);

    bool is_lowercase() const;
    bool is_uppercase() const;
    bool is_letter() const;
    bool is_digit() const;
    bool is_underscore() const;
    bool is_single_quote() const;
    bool is_double_quote() const;
    bool is_open_parenthesis() const;
    bool is_close_parenthesis() const;
    bool is_open_bracket() const;
    bool is_close_bracket() const;
    bool is_dot() const;
    bool is_comma() const;
    bool is_front_slash() const;
    bool is_back_slash() const;
    bool is_plus() const;
    bool is_minus() const;
    bool is_star() const;
    bool is_equals() const;
    bool is_more() const;
    bool is_less() const;
    bool is_exclamation_mark() const;
    bool is_pipe() const;
    bool is_whitespace() const;
    bool is_line_feed() const;
    bool is_carriage_return() const;
    bool is_newline_character() const;

    operator char() const;
};

template <>
struct std::hash<FancyChar>
{
    std::size_t operator()(FancyChar ch);
};
