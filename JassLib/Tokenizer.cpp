#include "Tokenizer.hpp"

#include <set>

#include "StringSet.hpp"

namespace Jass
{
	Token::Token() :
		start_(-1),
		stop_(-1),
		type_(TOKEN_EOF)
	{
	}

	Token::Token(
		FancyString value,
		int start,
		int stop,
		TokenType type,
		std::vector<Token> nested_tokens
	) :
		value_(std::move(value)),
		start_(start),
		stop_(stop),
		type_(type),
		nested_tokens_(std::move(nested_tokens))
	{

	}

	FancyString const& Token::value() const
	{
		return value_;
	}

	int Token::start() const
	{
		return start_;
	}

	int Token::stop() const
	{
		return stop_;
	}

	TokenType Token::type() const
	{
		return type_;
	}

	bool Token::is_eof() const
	{
		return type() == TOKEN_EOF;
	}

	bool Token::is_comment() const
	{
		return type() == TOKEN_COMMENT_BLOCK || type() == TOKEN_COMMENT_LINE || type() == TOKEN_PREPROCESSOR_COMMENT;
	}

	bool Token::is_whitespace() const
	{
		return type() == TOKEN_NEWLINE;
	}

	bool Token::is_identifier() const
	{
		return type() == TOKEN_IDENTIFIER;
	}

	bool Token::is_identifier(FancyString const& identifier) const
	{
		return is_identifier() && identifier == value();
	}

	bool Token::is_keyword() const
	{
		return type() == TOKEN_KEYWORD;
	}

	bool Token::is_keyword(FancyString const& keyword) const
	{
		return is_keyword() && keyword == value();
	}

	std::vector<Token> const& Token::nested_tokens() const
	{
		return nested_tokens_;
	}

	int Token::length() const
	{
		return stop() - start();
	}

	FancyChar Tokenizer::at(int idx) const
	{
		if (idx < 0 || idx >= text_size())
		{
			return '\0';
		}

		return text_[idx];
	}

	Tokenizer::Tokenizer(FancyString str) :
		text_(std::move(str)),
		idx_(0)
	{
	}

	int Tokenizer::text_size() const
	{
		return text_.size();
	}

	Token Tokenizer::eof() const
	{
		return Token("", text_size(), text_size(), TOKEN_EOF);
	}

	Token Tokenizer::parse_name()
	{
		// Keywords that would be used for parsing
		// Highlighting would likely use additional ones
		static StringSet keywords
		{
			"globals", "endglobals",
			"type", "extends",
			"native", "takes", "returns",
			"function", "endfunction",
			"method", "endmethod", "operator",
			"struct", "endstruct",
			"interface", "endinterface",
			"module", "endmodule", "implement",
			"scope", "endscope", "initializer",
			"library", "endlibrary", "requires", "uses", "needs",
			"loop", "exitwhen", "endloop",
			"if", "then", "elseif", "else", "endif",
			"and", "or", "not",
			"private", "public", "static", "constant", "local",
			"set", "call", "return"
		};

		TokenType type = TOKEN_IDENTIFIER;

		int stop = idx_;
		while (at(stop).is_letter() || at(stop).is_underscore() || at(stop).is_underscore())
		{
			stop++;
		}

		FancyString value = text_.middle(idx_, stop);
		if (keywords.contains(value))
		{
			type = TOKEN_KEYWORD;
		}

		Token token(value, idx_, stop, type);

		idx_ = stop;

		return token;
	}

	Token Tokenizer::parse_comment_block()
	{
		TokenType type = TOKEN_COMMENT_BLOCK;
		int stop = idx_;
		if (at(stop).is_front_slash() && at(stop + 1).is_star())
		{
			stop += 2;
		}

		while (stop + 1 < text_size())
		{
			// TODO@Daniel:
			// Handle docstring parameters
			if (at(stop).is_star() && at(stop + 1).is_front_slash())
			{
				stop++;
				break;
			}
			else
			{
				stop++;
			}
		}
		stop++;

		FancyString value = text_.middle(idx_, stop);
		Token token(value, idx_, stop, type);

		idx_ = stop;

		return token;
	}

	Token Tokenizer::parse_string()
	{
		static FancyString slash_escapes = "\\nt\"";
		static FancyString pipe_escapes = "cnr";

		TokenType type = TOKEN_STRING;
		std::vector<Token> nested_tokens;

		int stop = idx_;
		if (at(stop).is_double_quote())
		{
			stop++;
		}

		while (stop < text_size())
		{
			if (at(stop).is_double_quote())
			{
				stop++;
				break;
			}

			// Iirc, there are only single character escapes
			bool is_slash_escape = at(stop).is_back_slash() && slash_escapes.contains(at(stop + 1));
			bool is_pipe_escape = at(stop).is_pipe() && pipe_escapes.contains(at(stop + 1));
			if (is_pipe_escape || is_slash_escape)
			{
				FancyString value = text_.middle(stop, stop + 2);
				Token token(value, stop, stop + 2, TOKEN_ESCAPE_SEQUENCE);
				nested_tokens.push_back(token);

				stop++;
			}
			stop++;
		}

		FancyString value = text_.middle(idx_, stop);
		Token token(value, idx_, stop, type, nested_tokens);

		idx_ = stop;

		return token;
	}

	Token Tokenizer::parse_rawcode()
	{
		TokenType type = TOKEN_RAWCODE;
		int stop = idx_;
		if (at(stop).is_single_quote())
		{
			stop++;
		}

		while (stop < text_size())
		{
			// Iirc, there are no escape sequences in rawcodes
			if (at(stop).is_single_quote())
			{
				stop++;
				break;
			}
			else
			{
				stop++;
			}
		}

		FancyString value = text_.middle(idx_, stop);
		Token token(value, idx_, stop, type);

		idx_ = stop;

		return token;
	}

	Token Tokenizer::eat_all_of(TokenType token_type)
	{
		Token token = next();
		while (token.type() == token_type)
		{
			token = next();
		}
		return token;
	}

	Token Tokenizer::eat_until(TokenType token_type)
	{
		Token token = next();
		while (token.type() != token_type && !token.is_eof())
		{
			token = next();
		}
		return token;
	}

	Token Tokenizer::eat_comment_blocks()
	{
		return eat_all_of(TOKEN_COMMENT_BLOCK);
	}

	Token Tokenizer::next()
	{
		// Skipping leading whitespace
		while (at(idx_).is_whitespace() && !at(idx_).is_newline_character())
		{
			idx_++;
		}

		if (idx_ >= text_size())
		{
			return eof();
		}

		int stop = idx_ + 1;

		TokenType type = TOKEN_OTHER;

		std::vector<Token> nested_tokens;
		// TODO@Daniel:
		// This could be done much cleaner

		FancyChar first = at(idx_);
		FancyChar second = at(idx_ + 1);
		FancyChar third = at(idx_ + 2);

		if (first.is_front_slash())
		{
			if (second.is_front_slash())
			{
				if (third.is_exclamation_mark())
				{
					type = TOKEN_PREPROCESSOR_COMMENT;
					stop = idx_ + 3;
				}
				else
				{
					type = TOKEN_COMMENT_LINE;
					stop = idx_ + 2;
				}

				while (!at(stop).is_carriage_return() && !at(stop).is_carriage_return())
				{
					stop++;
				}
			}
			else if (second.is_star())
			{
				return parse_comment_block();
			}
			else if (second.is_equals())
			{
				type = TOKEN_ASSIGN_DIV;
				stop = idx_ + 2;
			}
			else
			{
				type = TOKEN_DIV;
			}
		}
		else if (first.is_double_quote())
		{
			return parse_string();
		}
		else if (first.is_single_quote())
		{
			return parse_rawcode();
		}
		else if (first.is_line_feed())
		{
			type = TOKEN_NEWLINE;
		}
		else if (first.is_carriage_return())
		{
			type = TOKEN_NEWLINE;
			if (second.is_line_feed())
			{
				stop = idx_ + 2;
			}
		}
		else if (first.is_comma())
		{
			type = TOKEN_COMMA;
		}
		else if (first.is_dot())
		{
			type = TOKEN_DOT;
		}
		else if (first.is_plus())
		{
			if (second.is_equals())
			{
				type = TOKEN_ASSIGN_ADD;
				stop = idx_ + 2;
			}
			else
			{
				type = TOKEN_ADD;
			}
		}
		else if (first.is_minus())
		{
			if (second.is_equals())
			{
				type = TOKEN_ASSIGN_SUB;
				stop = idx_ + 2;
			}
			else
			{
				type = TOKEN_SUB;
			}
		}
		else if (first.is_star())
		{
			if (second.is_equals())
			{
				type = TOKEN_ASSIGN_MUL;
				stop = idx_ + 2;
			}
			else
			{
				type = TOKEN_MUL;
			}
		}
		else if (first.is_equals())
		{
			if (second.is_equals())
			{
				type = TOKEN_EQUALS;
				stop = idx_ + 2;
			}
			else
			{
				type = TOKEN_ASSIGN;
			}
		}
		else if (first.is_less())
		{
			if (second.is_equals())
			{
				type = TOKEN_LESS_EQ;
				stop = idx_ + 2;
			}
			else
			{
				type = TOKEN_LESS;
			}
		}
		else if (first.is_more())
		{
			if (second.is_equals())
			{
				type = TOKEN_MORE_EQ;
				stop = idx_ + 2;
			}
			else
			{
				type = TOKEN_MORE;
			}
		}
		else if (first.is_letter() || first.is_underscore())
		{
			return parse_name();
		}
		else if (first.is_digit())
		{
			type = TOKEN_NUMBER;
			while (at(stop).is_digit())
			{
				stop++;
			}
		}

		FancyString value = text_.middle(idx_, stop);

		Token token(value, idx_, stop, type, nested_tokens);
		idx_ = stop;

		return token;
	}
}