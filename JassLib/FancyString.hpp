#pragma once

#include <string>

#include "FancyChar.hpp"

struct FancyString
{
private:
    std::string str_;

public:
    FancyString() = default;
    explicit FancyString(int size, FancyChar filler = '\0');
    FancyString(char const* str);
    FancyString(std::string str);

    int size() const;
    bool empty() const;
    bool contains(FancyChar ch) const;

    FancyChar at(int idx) const;
    FancyChar operator[](int idx) const;

    FancyString left(int stop) const;
    FancyString middle(int start, int stop) const;
    FancyString right(int start) const;

    bool operator<(FancyString const& other) const;
    bool operator<=(FancyString const& other) const;
    bool operator>(FancyString const& other) const;
    bool operator>=(FancyString const& other) const;
    bool operator==(FancyString const& other) const;

    FancyString operator+(FancyString const& other) const;
    FancyString& operator=(FancyString other);
    FancyString& operator+=(FancyString const& other);

    operator std::string() const;
};

template <>
struct std::hash<FancyString>
{
    std::size_t operator()(FancyString const& str) const;
};
