#pragma once

#include <string>
#include <vector>

#include "FancyChar.hpp"
#include "FancyString.hpp"

namespace Jass
{
    enum TokenType 
    {
        TOKEN_NUMBER,
        TOKEN_IDENTIFIER,
        TOKEN_KEYWORD,
        TOKEN_COMMENT_LINE,
        TOKEN_PREPROCESSOR_COMMENT,
        TOKEN_NEWLINE,
        TOKEN_COMMENT_BLOCK,

        TOKEN_OPEN_PAREN,
        TOKEN_CLOSE_PAREN,
        TOKEN_OPEN_BRACKET,
        TOKEN_CLOSE_BRACKET,

        TOKEN_COMMA,
        TOKEN_DOT,

        TOKEN_ADD,
        TOKEN_SUB,
        TOKEN_MUL,
        TOKEN_DIV,
        TOKEN_ASSIGN,
        TOKEN_ASSIGN_ADD,
        TOKEN_ASSIGN_SUB,
        TOKEN_ASSIGN_MUL,
        TOKEN_ASSIGN_DIV,

        TOKEN_LESS,
        TOKEN_LESS_EQ,
        TOKEN_MORE,
        TOKEN_MORE_EQ,
        TOKEN_EQUALS,

        TOKEN_STRING,
        TOKEN_RAWCODE,
        TOKEN_ESCAPE_SEQUENCE,
        TOKEN_WHITESPACE,
        TOKEN_OTHER,
        TOKEN_EOF
    };

    class Token
    {
    private:
        FancyString value_;
        int start_;
        int stop_;

        TokenType type_;

        // For escape sequences in strings, parameters in docstring and similar
        std::vector<Token> nested_tokens_;

    public:
        Token();
        Token(
            FancyString value,
            int start,
            int stop,
            TokenType type,
            std::vector<Token> nested_tokens = std::vector<Token>()
        );

        FancyString const& value() const;

        int start() const;
        int stop() const;

        TokenType type() const;

        bool is_eof() const;
        bool is_comment() const;
        bool is_whitespace() const;
        bool is_identifier() const;
        bool is_identifier(FancyString const& identifier) const;
        bool is_keyword() const;
        bool is_keyword(FancyString const& keyword) const;

        std::vector<Token> const& nested_tokens() const;

        int length() const;
    };

    // TODO@Daniel:
    // Add iterator for convenience
    class Tokenizer
    {
    private:
        // TODO@Daniel:
        // Support multiple 'files' instead of just one
        // Will help with block comments across files and such
        FancyString text_;

        int idx_;

        FancyChar at(int idx) const;

    public:
        Tokenizer(FancyString str);

        int text_size() const;

        Token eof() const;

        Token parse_name();
        Token parse_comment_block();
        Token parse_string();
        Token parse_rawcode();

        Token eat_all_of(TokenType token_type);
        Token eat_until(TokenType token_type);
        Token eat_comment_blocks();

        Token next();
    };
}